package com.group4.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group4Application {

	public static void main(String[] args) {
		SpringApplication.run(Group4Application.class, args);
	}

}
